module.exports = {
	parser: "@typescript-eslint/parser",
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/eslint-recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"plugin:react/recommended",
		"prettier/@typescript-eslint",
		"plugin:prettier/recommended",
		"plugin:react/recommended"
	],
	globals: {
		document: true,
		window: true,
		console: true
	},
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: "module",
		ecmaFeatures: {
			jsx: true // Allows for the parsing of JSX
		},
		project: "./tsconfig.json"
	},
	rules: {
		// ESLint Rules
		"array-bracket-spacing": [ "error", "never" ],
		"array-callback-return": [ "error", { allowImplicit: true } ],
		"block-scoped-var": "error",
		"brace-style": "off",
		"class-methods-use-this": "error",
		complexity: "warn",
		curly: [ "error", "multi" ],
		"default-case": "error",
		"default-param-last": "error",
		"dot-location": [ "error", "property" ],
		eqeqeq: "error",
		"for-direction": "off",
		"max-classes-per-file": [ "error", 1 ],
		"max-len": [ "error", 80 ],
		"no-alert": "warn",
		"no-dupe-else-if": "error",
		"no-else-return": "error",
		"no-empty-function": "off",
		"no-extra-parens": "off",
		"no-floating-decimal": "error",
		"no-magic-numbers": "off",
		"no-multi-spaces": "error",
		"no-param-reassign": "error",
		"no-template-curly-in-string": "error",
		"no-unused-expressions": "off",
		quotes: "off",
		semi: "off",
		"space-before-function-paren": "off",
		"vars-on-top": "error",
		"wrap-iife": "error",
		yoda: "error",

		// React Rules

		// TypeScript Rules
		"@typescript-eslint/brace-style": [
			"error",
			"1tbs",
			{ allowSingleLine: true }
		],
		"@typescript-eslint/consistent-type-definitions": [ "error", "interface" ],
		"@typescript-eslint/indent": [ "error", 2 ],
		"@typescript-eslint/no-empty-function": "error",
		"@typescript-eslint/no-explicit-any": "error",
		"@typescript-eslint/no-extra-parens": [ "error" ],
		"@typescript-eslint/no-magic-numbers": [
			"error",
			{ ignoreNumericLiteralTypes: true }
		],
		"@typescript-eslint/no-unnecessary-condition": "error",
		"@typescript-eslint/no-untyped-public-signature": "error",
		"@typescript-eslint/no-unused-expressions": [ "error" ],
		"@typescript-eslint/quotes": [ "error", "double" ],
		"@typescript-eslint/semi": [ "error", "never" ],
		"@typescript-eslint/space-before-function-paren": [ "error", "always" ]
	},
	settings: {
		react: {
			version: "detect"
		}
	}
}
