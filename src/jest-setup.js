import Enzyme from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import * as Aphrodite from "aphrodite"

Aphrodite.StyleSheetTestUtils.suppressStyleInjection()
Enzyme.configure({ adapter: new Adapter() })
