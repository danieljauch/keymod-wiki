module.exports = {
	collectCoverageFrom: [
		"**/*.ts",
		"**/*.tsx",
		"!**/coverage/**",
		"!**/*.snap",
		"!**/*.json",
		"!**/*.config.js",
		"!/index.js",
		"!/socket.js"
	],
	moduleFileExtensions: [ "ts", "tsx", "js", "jsx" ],
	reporters: [ "default" ],
	roots: [ "<rootDir>/js" ],
	setupFiles: [ "<rootDir>/jest-setup.js" ],
	setupFilesAfterEnv: [ "jest-enzyme" ],
	snapshotSerializers: [ "enzyme-to-json/serializer" ],
	testEnvironment: "enzyme",
	testPathIgnorePatterns: [ ".*/node_modules", ".*/coverage" ],
	testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(j|t)sx?$",
	transform: {
		"^.+\\.(j|t)sx?$": "ts-jest"
	}
}
